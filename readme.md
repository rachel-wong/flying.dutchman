# :checkered_flag: Tracking the Flying Dutchmen: following the International Space Station (ISS) & Hubble Space Telescope (HST)

> Using [leaflet.js](https://leafletjs.com) instead of google maps.

![walkthru.gif](walkthru.gif)

## :8ball: The APIs

NASA's [TLE Api](https://data.ivanstanojevic.me/api/tle/docs#operation/tle-record) because I was unable to bypass the mixed content error using [Open Notify](http://open-notify.org/Open-Notify-API/ISS-Location-Now/) when deployed.

[Wikipedia API](https://www.mediawiki.org/wiki/API:Main_page) has grown up a lot.

## :cyclone: The Approach

* In the beginning there was a map initialised with two markers.

* When the page has fully loaded, two separate API calls are made to NASA's TLE api for coordinates. 

`window.addEventListener('load', () => { // do stuff})` makes this happen.

* While the window is loading and DOM is being populated, separate api calls are being made to two different public apis every 3 seconds for the coordinates for HST and ISS. They are mapped directly onto the leaflet.js and you can see them track slowly across the map.

## :construction: To-dos

* Attempted to move into using axios, unsuccessful.

* Attempted to use IIFE to initiate maps and make fetch Api calls but not successful either.

* Was not able to find away to create a flight path using continuous data from one hour before and ahead of current time. It would be interesting to see if a curved path can be generated by smoothing over discrete coordinate points.

* Could not resolve the **mixed content error** with Open Notify's `http://` api calls. The data parses without errors on `localhost` live server, but once Netlify deploys, the Api call could not be made. Have attempted using header `Content-Security-Policy-Report-Only` and also use jquery method to no avail. :see_no_evil:

![mixed content error](mixedContent.png)

Without `https://` in Open Notify api call

![http error](http.png)

Modified to call Open Notify with `https://`

![https error](https.png)

* The only solution was to revert back to using NASA's TLE api to grab the coordinates. I contacted Open Notify API's owner Nathan Bergey who confirmed the (historical) mixed content error and there are currently no plans in place to upgrade it to https.

![Nathan Begley response](nathan.png)

## :eyes: Some observations

* Async/await throws up errors inside the `forEach` loop, even if the function is declared `async`. Need to work out why this happens.

* Most of the errors during dev  come from promises not resolving properly/on time. There is still a gap between what I think I understand and what I am to put into practice. I need to see what are better ways of making efficient (multiple, codependent) api calls within the same application.

* Leaflet.js is fantastic to use and in some ways, trump google maps. I'm satisfied I was able to riff from [major.Tom](https://bitbucket.org/rachel-wong/major.tom/src/master/) project.

* Would like to move on into using Node.JS and do some webscraping and making my own API.

## :gift: Other Resources

[Bootstrap CDN](https://getbootstrap.com/) for getting dressed quickly.

[Leaflet.js](https://leafletjs.com/examples/quick-start/) is a bonafide dreamboat - mwah :kissing:. Whoever wrote the documentation is a total bae.

[Undraw](https://undraw.co/search)

[Satellite.js](https://github.com/shashwatak/satellite-js) again because HST doesn't come with Open.Notify API so we have to convert the NASA TLEs into coordinates.

[MapBox](https://mapbox.com/pricing/) for map data.

The icons of the HST and ISS are from google.