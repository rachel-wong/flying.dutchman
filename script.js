  const astroContent = document.getElementById('wiki_container')
  const MAPBOX = "pk.eyJ1IjoicmFjaGVsd29uZyIsImEiOiJjazBsemE1amgxMWt5M2Rxb3NkNXR5YWt1In0.CXpT4HJUOrUVVdckl6piMA"

  // Fetch astronauts and wikipedia data when page finishes loading
  window.addEventListener('load', () => {
    async function getAstronauts() {
      let astroAPI = "http://api.open-notify.org/astros.json"
      let astroRes = await fetch(astroAPI)
      let astro = await astroRes.json()

      if (astro) {
        astro.people.forEach(person => {
          // get Astronaut wikipedia info
          let astroName = person.name.replace(" ", "_")
          // console.log('astroName:', astroName)
          let wikiAPI = `https://en.wikipedia.org/api/rest_v1/page/summary/${astroName}`

          // for some reasons await doesn't work inside the forEach. Revert to .then structure
          fetch(wikiAPI)
            .then(response => {
              return response.json()
            }).then(data => {
              if (data == undefined) {
                console.error("Data throws undefined")
              } else {
                // console.log("data", data)
                let astroWiki = {
                  name: data.title,
                  image_url: data.thumbnail ? data.thumbnail.source : "default_astro.svg", // default image some wiki json dont return an image even if one exists on the article ¯\_(ツ)_/¯
                  extract: data.extract,
                  url: data.content_urls.desktop.page
                }
                // console.log("astroWiki:", astroWiki)
                displayAstro(astroWiki)
              }
            }).catch(err => {
              console.error(err)
            })
        })
      } else {
        alert("No one is up there right now.")
      }
    }
    getAstronauts()
  })
  // Initialise leafletJS map
  var mymap = L.map('mapid').setView([0, 0], 2)

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + MAPBOX, {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: MAPBOX
  }).addTo(mymap)

  // class of icons
  const SatelliteIcon = L.Icon.extend({
    options: {
      iconSize: [40, 40],
      iconAnchor: [20, 20],
      popupAnchor: [3, -6]
    }
  })

  // setting separate icons for hubble and iss
  let issIcon = new SatelliteIcon({
    iconUrl: 'iss_icon.png'
  })

  const hubbleIcon = new SatelliteIcon({
    iconUrl: 'hubble_icon.png'
  })

  // creating markers with the right icons
  let issMarker = L.marker([0, 0], {
    icon: issIcon
  }).addTo(mymap)

  let hubbleMarker = L.marker([0, 0], {
    icon: hubbleIcon
  }).addTo(mymap)

  // Pop ups appearing onclick
  issMarker.bindPopup("<b>I am ISS<b>").openPopup()
  hubbleMarker.bindPopup("<b>Hey there I'm Hubble!</b>").openPopup()

  async function fetchData() {
    // Get ISS current position
    let issAPI = "https://ivanstanojevic.me/api/tle/25544"
    let issRes = await fetch(issAPI)
    let iss = await issRes.json()

    // Get HST TLE data
    let hubbleAPI = "https://ivanstanojevic.me/api/tle/20580"
    let hubbleRes = await fetch(hubbleAPI)
    let hubble = await hubbleRes.json()

    // Map coordinates on leaflet.js map
    if (iss && hubble) {
      let iss_coords = convertTLE(iss.line1, iss.line2)
      issMarker.setLatLng([iss_coords.latitude, iss_coords.longitude])
      let hubble_coords = convertTLE(hubble.line1, hubble.line2)
      hubbleMarker.setLatLng([hubble_coords.latitude, hubble_coords.longitude])
    } else {
      alert("No data available for Hubble Space telescope and International Space Station")
    }
  }

  // Convert HST TLE using Satellite.js to get longitude and latitude
  function convertTLE(line1, line2) {
    // Initialize a satellite record
    var satrec = satellite.twoline2satrec(line1, line2)
    //  Or you can use a JavaScript Date
    var positionAndVelocity = satellite.propagate(satrec, new Date())
    // The position_velocity result is a key-value pair of ECI coordinates.
    // These are the base results from which all other coordinates are derived.
    var positionEci = positionAndVelocity.position
    var gmst = satellite.gstime(new Date())
    let positionGd = satellite.eciToGeodetic(positionEci, gmst)
    let longitude = positionGd.longitude
    let latitude = positionGd.latitude
    let longitudeStr = satellite.degreesLong(longitude)
    let latitudeStr = satellite.degreesLat(latitude)

    return {
      longitude: longitudeStr,
      latitude: latitudeStr
    }
  }

  // Display astronaut wiki data onto DOM on pageload
  function displayAstro(astronaut) {
    var {
      name,
      image_url,
      extract,
      url
    } = astronaut

    let card = document.createElement('div')
    card.classList.add('card', 'astronaut_card')

    let profileImage = document.createElement('img')
    profileImage.classList.add('card-img-top', 'astronaut_image')
    profileImage.src = image_url

    let cardBody = document.createElement('div')
    cardBody.classList.add('card-body')

    let header = document.createElement('h5')
    header.classList.add('card-title', 'font-weight-bold', 'astronaut_name')
    header.innerHTML = name

    let extractText = document.createElement('p')
    extractText.classList.add('card-text', 'astronaut_extract')
    extractText.innerHTML = extract

    let button = document.createElement('a')
    button.classList.add('btn', 'btn-info', 'btn-sm', 'astronaut_wikilink')
    button.href = url
    button.innerHTML = `Read about ${name.split(" ")[0]}`

    cardBody.appendChild(header)
    cardBody.appendChild(extractText)
    cardBody.appendChild(button)

    card.appendChild(profileImage)
    card.appendChild(cardBody)

    astroContent.appendChild(card)
  }

  // Not fetchData(), because the () makes it run immediately
  // Get the data every 2 secs so it looks like HST & ISS are tracking across the map
  setInterval(fetchData, 2000)